/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   by: spuyet <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   created: 2013/12/03 12:41:57 by spuyet            #+#    #+#             */
/*   Updated: 2013/12/08 19:32:41 by spuyet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft.h"

static char		**ft_explode(char const *s, char c, size_t n, size_t index)
{
	size_t			line;
	char			**tab;
	char			*ptr;

	tab = (char **) malloc((n + 1) * sizeof(char *));
	line = 0;
	if (!s || !tab)
		return (NULL);
	while (s[index])
	{
		ptr = ft_strchr(&(s[index]), c);
		if (!ptr)
			ptr = ft_strchr(&(s[index]), '\0');
		tab[line] = ft_strsub(s, index, (size_t)(ptr - &(s[index])));
		index = index + ft_strlen(tab[line]) + 1;
		if (s[index] == c)
		{
			tab[++line] = ft_strnew(0);
			index++;
		}
		line++;
	}
	tab[n] = 0;
	return (tab);
}

static size_t	nbr_lines(char *str)
{
	size_t			line;
	size_t			index;

	line = 0;
	index = 0;
	while (str[index] != '\0')
	{
		if (str[index] == '\n')
			line++;
		index++;
	}
	return (line);
}

static t_file	*new_element(int fd, char *buf, int size)
{
	t_file			*lst;
	int				tmp;
	char			*str;

	str = ft_strnew(0);
	lst = (t_file *) malloc(sizeof(t_file));
	if (!lst)
		return (NULL);
	lst->current_line = 0;
	lst->file_descriptor = fd;
	while ((tmp = read(fd, buf, size)))
		str = ft_strnjoin(str, buf, tmp);
	if (!*str)
	{
		lst->max_line = 0;
		lst->content_file = NULL;
		return (lst);
	}
	lst->max_line = nbr_lines(str);
	lst->content_file = ft_explode(str, '\n', lst->max_line, lst->current_line);
	free(str);
	free(buf);
	if (!lst->content_file)
		return (NULL);
	return (lst);
}

static int	check_filedescriptor(t_file *list, int fd, char **line, int size)
{
	char	*buf;

	buf = (char *) malloc(size);
	if (!buf)
		return (-1);
	while (list->next && list->file_descriptor != fd)
		list = list->next;
	if (list->file_descriptor == fd)
	{
		if (list->current_line >= list->max_line)
			return (0);
		*line = ft_strdupf(list->content_file[list->current_line++]);
		if (!line)
			return (-1);
		return (1);
	}
	list->next = new_element(fd, buf, size);
	list = list->next;
	*line = ft_strdupf(list->content_file[list->current_line++]);
	if (!line)
		return (-1);
	if (list->current_line >= list->max_line)
		return (0);
	return (1);
}

int			get_next_line(int const fd, char **line)
{
	static t_file	*list;
	int				size;
	char			*buf;

	if (fd < 0)
		return (-1);
	*line = ft_strnew(0);
	if (BUF_SIZE > 8000000)
		size = 4096;
	else
		size = (int) BUF_SIZE;
	if (list)
		return (check_filedescriptor(list, fd, line, size));
	buf = (char *) malloc(size);
	list = new_element(fd, buf, size);
	if (!buf)
		return (-1);
	return (check_filedescriptor(list, fd, line, size));
}
