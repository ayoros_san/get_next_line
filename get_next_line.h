/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spuyet <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/03 12:34:26 by spuyet            #+#    #+#             */
/*   Updated: 2013/12/08 19:13:47 by spuyet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUF_SIZE 4096
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
typedef struct				s_file
{
	char					**content_file;
	size_t					current_line;
	size_t					max_line;
	int						file_descriptor;
	struct s_file			*next;
}							t_file;

int		get_next_line(int const fd, char **line);
#endif
