/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spuyet <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 18:31:47 by spuyet            #+#    #+#             */
/*   Updated: 2013/12/03 11:40:45 by spuyet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new_list;
	new_list = NULL;
	if (lst)
	{
		new_list = (*f)(lst);
		if (lst->next)
			new_list->next = ft_lstmap((lst->next), f);
	}
	return (new_list);
}
